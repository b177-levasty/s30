let express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.szjez.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Set notification for connection succes or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is succesful, output  a message in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Create a task schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// default values are the predefined values for a field
		default: "pending"
	}
})

// Create Models 
// Server > Schema > Database > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);

// Sign-up Schema Activity
const signUpSchema = new mongoose.Schema({
	username: String,
	password: String
})
// Sign-up Model Activity
const signUp = mongoose.model("Sign-up", signUpSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){
			// Returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				}
				// If no error faound while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	}) 
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		//If an error occured
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// Sign-up POST Activity
app.post("/signup", (req, res) => {
	signUp.findOne({username : req.body.username}, (err, result) => {
		if(req.body.username == "" && req.body.password == ""){
			return res.send("Please enter a username or password")
		}
		else if(req.body.username == ""){
			return res.send("Please enter a username")
		}
		else if(req.body.password == ""){
			return res.send("Please enter a password")
		}
		else if(result != null && result.username == req.body.username){
			return res.send("The username that you entered is already used, please enter a new one.")
		}
		else{
			let newSignUp = new signUp({
				username : req.body.username,
				password : req.body.password
			})

			newSignUp.save((saveErr, savedAccount) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.status(201).send("New account has been created");
				}
			})
		}
	}) 
})


// Testing lang
/*app.post("/singup", (req, res) => {
	signUp.findOne({password : req.body.password}, (err, result) => {
			let newPass = new signUp({
				password : req.body.password
			})
			newPass.save
})*/

// Stretch goal
app.get("/signup", (req, res) => {
	signUp.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})



app.listen(port, () => console.log(`Server running at port ${port}`));